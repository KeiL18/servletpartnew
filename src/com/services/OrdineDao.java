package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.connessione.ConnettoreDB;
import com.models.Ordine;
import com.models.Prodotto;
import com.models.Utente;

public class OrdineDao implements Dao<Ordine> {

	public Utente getUtenteByRef() {
		return null;
	}

	@Override
	public Ordine getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idOrdine,codice,indirizzo,refUtente FROM ordine WHERE idOrdine = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Ordine temp = new Ordine();
		temp.setIdOrdine(risultato.getInt(1));
		temp.setCodice(risultato.getString(2));
		temp.setIndirizzo(risultato.getString(3));
		temp.setRefUtente(risultato.getInt(4));
//		int refUtente = risultato.getInt(4);

//   	temp.setUtente(getUtenteByRef(refUtente));
//		String query1 = "SELECT idProdotto,nome,codice,descrizione,prezzo,quantita FROM prodotto "
//				+ " JOIN ProdottoOrdine ON prodotto.idProdotto=ProdottoOrdine.refProdotto"
//				+ "  JOIN ordine ON ProdottoOrdine.refOrdine=ordine.idOrdine WHERE idOrdine=?";
//		PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement(query1);
//		ps1.setInt(1, id);
//		ResultSet ris = ps.executeQuery();
//		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
//		while (ris.next()) {
//			int id1 = ris.getInt(1);
//			String nome = ris.getString(2);
//			String codice = ris.getString(3);
//			String descrizione = ris.getString(4);
//			Float prezzo = ris.getFloat(5);
//			int quantita = ris.getInt(6);
//
//			Prodotto pr = new Prodotto(id1, nome, codice, descrizione, prezzo, quantita);
//			elenco.add(pr);
//		}
//
//		temp.setElenco(elenco);

		return temp;

	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idOrdine,codice,indirizzo,refUtente FROM ordine";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {

			Ordine temp = new Ordine();
			temp.setIdOrdine(risultato.getInt(1));
			temp.setCodice(risultato.getString(2));
			temp.setIndirizzo(risultato.getString(3));
			temp.setRefUtente(risultato.getInt(4));
//			int refUtente = risultato.getInt(4);

//   		temp.setUtente(getUtenteByRef(refUtente));
//			String query1 = "SELECT idProdotto,nome,codice,descrizione,prezzo,quantita FROM prodotto "
//					+ " JOIN ProdottoOrdine ON prodotto.idProdotto=ProdottoOrdine.refProdotto"
//					+ "  JOIN ordine ON ProdottoOrdine.refOrdine=ordine.idOrdine WHERE idOrdine=?";
//			PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement(query1);
//			ps1.setInt(1, temp.getIdOrdine());
//			ResultSet ris = ps.executeQuery();
//			ArrayList<Prodotto> elenco1 = new ArrayList<Prodotto>();
//			while (ris.next()) {
//				int id1 = ris.getInt(1);
//				String nome = ris.getString(2);
//				String codice = ris.getString(3);
//				String descrizione = ris.getString(4);
//				Float prezzo = ris.getFloat(5);
//				int quantita = ris.getInt(6);
//
//				Prodotto pr = new Prodotto(id1, nome, codice, descrizione, prezzo, quantita);
//				elenco1.add(pr);
//			}
//
//			temp.setElenco(elenco1);

			elenco.add(temp);
		}
		return elenco;

	}
	
	public Ordine getByCod(String varCod) throws SQLException{
		Ordine temp = new Ordine();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT idOrdine,codice,indirizzo,refUtente FROM ordine WHERE codice = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, varCod);
		
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		
		temp.setIdOrdine(risultato.getInt(1));
		temp.setCodice(risultato.getString(2));
		temp.setIndirizzo(risultato.getString(3));
		temp.setRefUtente(risultato.getInt(4));
			
		return temp;
	}

	@Override
	public Ordine insert(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO ordine (codice,indirizzo,refUtente) VALUES (?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getCodice());
		ps.setString(2, t.getIndirizzo());
		ps.setInt(3, t.getRefUtente());

		// TODO: Usare il Dao prodotti per inserire i prodotti associati all'ordine.

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

		t.setIdOrdine(risultato.getInt(1));
		
		return t;
	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM ordine WHERE idOrdine = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getIdOrdine());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}
	
	public void deleteProdottiOrdine(Ordine t) throws SQLException{
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM ProdottoOrdine WHERE refOrdine = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getIdOrdine());

		ps.executeUpdate();
	}

	@Override
	public Ordine update(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE ordine SET codice = ?, indirizzo=? WHERE idOrdine = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getCodice());
		ps.setString(2, t.getIndirizzo());
		ps.setInt(3, t.getIdOrdine());
		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getIdOrdine());

		return null;
	}
	
	public void insertProdottiOrdine(ArrayList<Prodotto> listaCarrello, int IDordine) throws SQLException{
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		for(int i = 0 ; i<listaCarrello.size(); i++) {
			Prodotto prodTemp = listaCarrello.get(i);
			
			String query = "INSERT INTO ProdottoOrdine (refProdotto, refOrdine) VALUES (?,?)";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, prodTemp.getId_prodotto());
			ps.setInt(2, IDordine);
	
			ps.executeUpdate();
		}
	}

}
