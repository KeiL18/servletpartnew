package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.models.Prodotto;
import com.connessione.ConnettoreDB;

public class ProdottoDAO implements Dao<Prodotto> {
	
	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idProdotto, nome, codice, descrizione, prezzo, quantita FROM prodotto WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setId_prodotto(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			temp.setPrezzo(risultato.getFloat(5));
			temp.setQuantita(risultato.getInt(6));
			elenco.add(temp);
		}

		return elenco;
	}

	public Prodotto findByCodice(String varCodice) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idProdotto, nome, codice, descrizione, prezzo, quantita FROM prodotto WHERE codice = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, varCodice);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Prodotto temp = new Prodotto();
		temp.setId_prodotto(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setCodice(risultato.getString(3));
		temp.setDescrizione(risultato.getString(4));
		temp.setPrezzo(risultato.getFloat(5));
		temp.setQuantita(risultato.getInt(6));

		return temp;
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "DELETE FROM prodotto WHERE codice = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getCodice());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	public Prodotto update(Prodotto objProdotto) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "UPDATE Prodotto SET nome = ?, codice = ?, descrizione = ?, prezzo=?, quantita=? WHERE idProdotto = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, objProdotto.getNome());
		ps.setString(2, objProdotto.getCodice());
		ps.setString(3, objProdotto.getDescrizione());
		ps.setFloat(4, objProdotto.getPrezzo());
		ps.setInt(5, objProdotto.getQuantita());
		ps.setInt(6, objProdotto.getId_prodotto());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return objProdotto;

		return null;
	}

	@Override
	public Prodotto insert(Prodotto objProdotto) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO Prodotto (nome, codi, descrizione, prezzo, codice, quantita) VALUE (?,?,?,?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, objProdotto.getNome());
		ps.setString(2, objProdotto.getCodice());
		ps.setString(3, objProdotto.getDescrizione());
		ps.setString(4, objProdotto.getCodice());
		ps.setFloat(5, objProdotto.getPrezzo());
		ps.setInt(6, objProdotto.getQuantita());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

		objProdotto.setId_prodotto(risultato.getInt(1));
		
		return objProdotto;
	}

	@Override
	public Prodotto getById(int ID) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}