package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.models.Utente;
import com.connessione.ConnettoreDB;

public class UtenteDAO implements Dao<Utente> {
	
	public Utente getByUser(String varUser) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idUtente, nome, cognome, email, nickname, passw, ruolo FROM utente WHERE nickname=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, varUser);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Utente temp = new Utente();
		temp.setId_utente(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setCognome(risultato.getString(3));
		temp.setEmail(risultato.getString(4));
		temp.setNickname(risultato.getString(5));
		temp.setPassword(risultato.getString(6));
		temp.setRuolo(risultato.getString(7));

		return temp;
	}
	
	@Override
	public Utente update(Utente objUtente) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "UPDATE utente SET nome = ?, cognome = ?, email = ?, nickname=?, passw=?, ruolo=? WHERE idUtente = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ps.setString(1, objUtente.getNome());
		ps.setString(2, objUtente.getCognome());
		ps.setString(3, objUtente.getEmail());
		ps.setString(4, objUtente.getNickname());
		ps.setString(5, objUtente.getPassword());
		ps.setString(6, objUtente.getRuolo());
		ps.setInt(7, objUtente.getId_utente());
		
		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return objUtente;

		return null;
	}

	public Utente insert(Utente objUtente) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO utente (nome, cognome, email, nickname, passw, ruolo) VALUE (?,?,?,?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, objUtente.getNome());
		ps.setString(2, objUtente.getCognome());
		ps.setString(3, objUtente.getEmail());
		ps.setString(4, objUtente.getNickname());
		ps.setString(5, objUtente.getPassword());
		ps.setString(6, objUtente.getRuolo());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

		objUtente.setId_utente(risultato.getInt(1));
		
		return objUtente;
	}

	public void checkCredentials(Utente objUtente) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idUtente, nome, cognome, email, nickname, passw, ruolo FROM utente WHERE nickname = ? AND passw = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, objUtente.getNickname());
		ps.setString(2, objUtente.getPassword());

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		objUtente.setId_utente(risultato.getInt(1));

		objUtente.setRuolo(risultato.getString(4));

	}

	@Override
	public Utente getById(int ID) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idUtente, nome, cognome, email, nickname, passw, ruolo FROM utente WHERE idUtente=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, ID);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Utente temp = new Utente();
		temp.setId_utente(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setCognome(risultato.getString(3));
		temp.setEmail(risultato.getString(4));
		temp.setNickname(risultato.getString(5));
		temp.setPassword(risultato.getString(6));
		temp.setRuolo(risultato.getString(7));

		return temp;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elenco = new ArrayList<Utente>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idUtente, nome, cognome, email, nickname, passw, ruolo FROM utente WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Utente temp = new Utente();
			temp.setId_utente(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCognome(risultato.getString(3));
			temp.setEmail(risultato.getString(4));
			temp.setNickname(risultato.getString(5));
			temp.setPassword(risultato.getString(6));
			temp.setRuolo(risultato.getString(7));
			elenco.add(temp);
		}

		return elenco;
	}

	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "DELETE FROM utente WHERE idUtente = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getId_utente());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}


public boolean deleteById(int id)  throws SQLException {
	Connection conn = ConnettoreDB.getIstanza().getConnessione();

	String query = "DELETE FROM utente WHERE idUtente = ?";
	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	ps.setInt(1, id);

	int affRows = ps.executeUpdate();
	if (affRows > 0)
		return true;

	return false;
}
	 
	
	
	
}