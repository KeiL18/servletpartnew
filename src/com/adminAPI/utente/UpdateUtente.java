package com.adminAPI.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Utente;
import com.services.UtenteDAO;
import com.utilities.ResponsoOperazione;

@WebServlet("/admin/utente/updateutente")
public class UpdateUtente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer idUtente = request.getParameter("idUtente") != null ? Integer.parseInt(request.getParameter("idUtente"))
				: -1;
		String nomeUtente = request.getParameter("nome") != null ? request.getParameter("nome") : "";
		String cognomeUtente = request.getParameter("cognome") != null ? request.getParameter("cognome") : "";
		String emailUtente = request.getParameter("email") != null ? request.getParameter("email") : "";
		String nicknameUtente = request.getParameter("nickname") != null ? request.getParameter("nickname") : "";
		String passwordUtente = request.getParameter("password") != null ? request.getParameter("password") : "";
		String ruoloUtente = request.getParameter("ruolo") != null ? request.getParameter("ruolo") : "";

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		UtenteDAO utDao = new UtenteDAO();

		if (idUtente == -1 || nomeUtente.trim().equals("") || cognomeUtente.trim().equals("")
				|| nicknameUtente.trim().equals("") || passwordUtente.trim().equals("")
				|| ruoloUtente.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "campi mancanti")));
			return;
		}

		try {
			Utente temp = utDao.getById(idUtente);
			temp.setNome(nomeUtente);
			temp.setCognome(cognomeUtente);
			temp.setEmail(emailUtente);
			temp.setNickname(nicknameUtente);
			temp.setPassword(passwordUtente);
			temp.setRuolo(ruoloUtente);

			temp = utDao.update(temp);
			
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/listUtenti.jsp");

		} catch (SQLException e) {
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "impossibile eseguire l'operazione")));
		}
	}
}