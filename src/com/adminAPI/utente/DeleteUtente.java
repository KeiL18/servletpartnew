package com.adminAPI.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.services.UtenteDAO;
import com.utilities.ResponsoOperazione;



@WebServlet("/Admin/utente/delete")
public class DeleteUtente extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");		
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();
		
		Integer identificatore = request.getParameter("idUtente") != null ? Integer.parseInt(request.getParameter("idUtente")) : -1;
		
		if(identificatore != -1) {

			UtenteDAO ut_dao = new UtenteDAO();
try {
				
				if(ut_dao.deleteById(identificatore)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(jsonizzatore.toJson(res));
					response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/listUtenti.jsp");
				}
				else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a trovare l'utente");
					out.print(jsonizzatore.toJson(res));
					response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
				}
				
			} catch (SQLException e) {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
				out.print(jsonizzatore.toJson(res));
				response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
			}
			
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
			out.print(jsonizzatore.toJson(res));
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
		}
	}

}
