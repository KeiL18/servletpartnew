package com.adminAPI.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Utente;
import com.services.UtenteDAO;

@WebServlet("/admin/utente/insertutente")
public class CreateUtente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String varNome = request.getParameter("input_nome");
		String varCognome = request.getParameter("input_cognome");
		String varEmail = request.getParameter("input_email");
		String varNickname = request.getParameter("input_nickname");
		String varPassword = request.getParameter("input_password");
		String varRuolo = request.getParameter("input_ruolo");

		Utente temp = new Utente();
		temp.setNome(varNome);
		temp.setCognome(varCognome);
		temp.setEmail(varEmail);
		temp.setNickname(varNickname);
		temp.setPassword(varPassword);
		temp.setRuolo(varRuolo);

		UtenteDAO ut_dao = new UtenteDAO();
		
		try {
			ut_dao.insert(temp);
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/listUtenti.jsp");
		} catch (SQLException e) {
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
			System.out.println( e.getMessage());
			response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/errore.html");
		}


	}

}