package com.adminAPI.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Utente;
import com.services.UtenteDAO;

@WebServlet("/admin/utente/listutenti")
public class ListUtenti extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			UtenteDAO utDao = new UtenteDAO();
			
			Integer identificatore = request.getParameter("id_utente") != null ? Integer.parseInt(request.getParameter("id_utente")) : -1;
			
			try {
				
				if(identificatore == -1) {
					ArrayList<Utente> elencoUtenti = utDao.getAll();
					
					String risultatoJson = new Gson().toJson(elencoUtenti);
					
					out.print(risultatoJson);
				}
				else {
					Utente temp = utDao.getById(identificatore);
					out.print(new Gson().toJson(temp));
				}

			} catch (SQLException e) {

				System.out.println(e.getMessage());
				
			}
	}

}
