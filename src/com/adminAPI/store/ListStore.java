package com.adminAPI.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Store;
import com.services.StoreDao;

/**
 * Servlet implementation class ListStore
 */
@WebServlet("/admin/store/ListStore")
public class ListStore extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Gson jasonizzatore=new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		StoreDao sdao=new StoreDao();
		try {
			ArrayList<Store> elencoTemp=sdao.getAll();
			out.print(jasonizzatore.toJson(elencoTemp));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
