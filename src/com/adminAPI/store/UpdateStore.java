package com.adminAPI.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Store;
import com.services.StoreDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/admin/store/UpdateStore")
public class UpdateStore extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome=request.getParameter("inputNome");
		String pIva=request.getParameter("inputPiva");
		String nomeUtente=request.getParameter("inputNomeUtente");
		String passwordStore=request.getParameter("inputPassword");
		String emailStore=request.getParameter("inputEmail");
		
		Store temp=new Store(nome,pIva,nomeUtente,passwordStore,emailStore);
		
		StoreDao sdao=new StoreDao();
		PrintWriter out=response.getWriter();
		
		try {
			Store tempAgg=sdao.update(temp);
			
			if((Integer) tempAgg.getIdStore()!=null) {
				response.sendRedirect("listShop.jsp");
			}
			else {
				response.sendRedirect("errore.html");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
