package com.adminAPI.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.models.Store;
import com.services.StoreDao;

/**
 * Servlet implementation class DeleteStore
 */
@WebServlet("/admin/store/DeleteStore")
public class DeleteStore extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		Integer identificatore = request.getParameter("idStore") != null ? Integer.parseInt(request.getParameter("idStore")) : -1;
		
		StoreDao sdao=new StoreDao();
		try {
			Store temp=sdao.getById(identificatore);
			if(sdao.delete(temp)) {
				response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/listShop.jsp");
			}
			else {
				response.sendRedirect("http://localhost:8080/ECommerceDeliveryServlet/listShop.jsp");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
