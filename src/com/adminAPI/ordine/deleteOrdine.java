package com.adminAPI.ordine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Ordine;
import com.services.OrdineDao;
import com.utilities.ResponsoOperazione;

@WebServlet("/admin/ordine/delete")
public class deleteOrdine extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();
		
		String varCodice = request.getParameter("codiceOrdine") != null ? request.getParameter("codiceOrdine") : "";
		
		if(varCodice.isBlank()) {		
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Informazioni mancanti");
			out.print(new Gson().toJson(res));
			return;
		}
		
		OrdineDao oDAO = new OrdineDao();
		Ordine OrdineTemp = new Ordine();
		
		
		try {
			OrdineTemp = oDAO.getByCod(varCodice);
			
			if(oDAO.delete(OrdineTemp)) {
				ResponsoOperazione res = new ResponsoOperazione("OK", "");
				out.print(jsonizzatore.toJson(res));
			}
			else {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a trovare l'utente");
				out.print(jsonizzatore.toJson(res));
			}
		} catch (SQLException e1) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore di interrogazione DB");
			out.print(new Gson().toJson(res));
			e1.printStackTrace();
			return;
		}
	}
}
