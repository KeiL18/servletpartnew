package com.adminAPI.ordine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.models.Ordine;
import com.models.Prodotto;
import com.services.OrdineDao;
import com.services.UtenteDAO;
import com.utilities.ResponsoOperazione;

@WebServlet("/admin/ordine/create")
public class createOrdine extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UtenteDAO uDAO = new UtenteDAO();
		
		String varCodice = request.getParameter("codiceOrdine") != null ? request.getParameter("codiceOrdine") : "";
		String varIndirizzo = request.getParameter("indirizzoOrdine") != null ? request.getParameter("indirizzoOrdine") : "";
		int varUserID;
//		try {
			varUserID = 1; //(uDAO.getByUser(session.getAttribute("User").toString())).getId_utente();
//		} catch (SQLException e) {
//			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore di interrogazione DB");
//			out.print(new Gson().toJson(res));
//			e.printStackTrace();
//			return;
//		}
		ArrayList<Prodotto> listaProdotti = new ArrayList<Prodotto>(); //TODO:Popola l'arrayList con i prodotti presenti nel carrello
		
		//dati di prova
		Prodotto prod1 = new Prodotto(); prod1.setId_prodotto(1); listaProdotti.add(prod1);
		Prodotto prod2 = new Prodotto(); prod2.setId_prodotto(2); listaProdotti.add(prod2);
		
		if(varCodice.isBlank() || varIndirizzo.isBlank()) {		
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Informazioni mancanti");
			out.print(new Gson().toJson(res));
			return;
		}
		
		Ordine OrdineTemp = new Ordine();
		OrdineTemp.setCodice(varCodice);
		OrdineTemp.setIndirizzo(varIndirizzo);
		OrdineTemp.setRefUtente(varUserID);
		OrdineTemp.setElenco(listaProdotti);
		
		try {
			OrdineDao oDAO = new OrdineDao();
			OrdineTemp = oDAO.insert(OrdineTemp);
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(new Gson().toJson(res));
			
			oDAO.insertProdottiOrdine(listaProdotti, OrdineTemp.getIdOrdine());
		} catch (SQLException e1) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore di interrogazione DB");
			out.print(new Gson().toJson(res));
			e1.printStackTrace();
			return;
		}
	}
}
