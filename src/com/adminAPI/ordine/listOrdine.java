package com.adminAPI.ordine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.models.Ordine;
import com.services.OrdineDao;
import com.utilities.ResponsoOperazione;

@WebServlet("/admin/ordine/list")
public class listOrdine extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		ArrayList<Ordine> elencoOrdini = new ArrayList<Ordine>();
		OrdineDao oDAO = new OrdineDao();
		ResponsoOperazione res = null;
		Gson jsonizzatore = new Gson();
		
		try {
			elencoOrdini = oDAO.getAll();
			res = new ResponsoOperazione("OK", jsonizzatore.toJson(elencoOrdini));
			out.print(new Gson().toJson(res));
		} catch (SQLException e) {
			res = new ResponsoOperazione("Errore", "Errore nell'operazione!");
			out.print(new Gson().toJson(res));
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
}
