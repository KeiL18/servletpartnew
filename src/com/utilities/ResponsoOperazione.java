package com.utilities;

public class ResponsoOperazione{
	private String risultato;
	private String dettaglio;

	public ResponsoOperazione(String risultato, String dettaglio) {
		super();
		this.risultato = risultato;
		this.dettaglio = dettaglio;
	}

	public String getRisultato() {
		return risultato;
	}

	public void setRisultato(String risultato) {
		this.risultato = risultato;
	}

	public String getDettaglio() {
		return dettaglio;
	}

	public void setDettaglio(String dettaglio) {
		this.dettaglio = dettaglio;
	}
	
}
