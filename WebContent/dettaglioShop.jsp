<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.services.StoreDao"%>
<%@page import="com.models.Store"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifica Shop</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">

				<%
				String piva = (String) request.getParameter("pIva");

				StoreDao sdao = new StoreDao();
				Store temp = sdao.getById(sdao.getIdBypIva(piva));
				%>

				<form action="admin/store/UpdateStore" method="POST">
					<h1>Dettaglio Store</h1>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputPiva">Partita Iva:</label> <input type="text"
									class="form-control" id="inputPiva" name="inputPiva"
									value="<%out.print(temp.getpIva());%>" />

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="inputNome">Nome:</label> <input type="text"
									class="form-control" id="inputNome" name="inputNome"
									value="<%out.print(temp.getNome());%>" />
							</div>
						</div>
					</div>

	<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputNomeUtente">Username:</label> <input type="text"
				 					class="form-control" id="inputNomeUtente" name="inputNomeUtente"
									value="<%out.print(temp.getNomeUtente());%>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputPassword">Password:</label> <input type="text"
									class="form-control" id="inputPassword" name="inputPassword"
									value="<%out.print(temp.getPasswordStore());%>" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputEmail">Email:</label> <input type="text"
									class="form-control" id="inputEmail" name="inputEmail"
									value="<%out.print(temp.getNome());%>" />
							</div>
						</div>
					</div>

				
					<div class="row">
						<div class="col-md">
							<button type="submit" class="btn btn-warning btn-block"
								id="cta-inserisci">Modifica</button>
						</div>
					</div>
				</form>

				<div class="row">
					<div class="col-md">
						<form action="admin/store/DeleteStore" method="POST">
							<input type="text" class="form-control" id="idStore" name="idStore"
								value="<%out.print(temp.getIdStore());%>" />
							<div class="row">
								<div class="col-md">
									<button type="submit" class="btn btn-danger btn-block"
										id="cta-inserisci">Elimina</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>

		<%-- <div class="row">
			<div class="col">
				<%
					/* String nome = "Giovanni";
				
					out.print(nome); */
					
					/* HttpSession sessione = request.getSession();
			
					out.print(sessione.getAttribute("stud_id"));
					out.print(sessione.getAttribute("stud_nome"));
					out.print(sessione.getAttribute("stud_cognome"));
					out.print(sessione.getAttribute("stud_matricola"));
					out.print(sessione.getAttribute("stud_sesso")); */
					
					
				%>
				
				
			</div>
		</div>	 --%>
	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>