<%@page import="com.models.Store"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.services.StoreDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Elenco Shop</title>
</head>
<body>

	<div class="container">
		<h1 class="text-center">Elenco Stores</h1>
		<div class="row">

			<div class="col-md-10">

				<table class="table">
					<thead>
						<tr>
							<th>Partita Iva</th>
							<th>Nome</th>
							<th>Email</th>
							<th>nomeUtente</th>
							<th>password</th>
						</tr>
					</thead>
					<tbody id="contenuto-richiesta">

						<%
						StoreDao sDao = new StoreDao();
						ArrayList<Store> elenco = sDao.getAll();

						String risultato = "";
						for (int i = 0; i < elenco.size(); i++) {
							Store temp = elenco.get(i);

							risultato += "<tr data-identificatore='" + temp.getIdStore() + "'>";
							//risultato += "    <td><a href='dettaglioShop.jsp?idStore=" + temp.getpIva() + ">"+temp.getpIva()+"</a></td>";
							risultato += "    <td><a href='dettaglioShop.jsp?pIva=" + temp.getpIva() + "'>" + temp.getpIva() + "</a></td>";
							risultato += "    <td>" + temp.getNome() + "</td>";
							risultato += "    <td>" + temp.getEmailStore() + "</td>";
							risultato += "    <td>" + temp.getNomeUtente() + "</td>";
							risultato += "    <td>" + temp.getPasswordStore() + "</td>";
							risultato += "</tr>";
						}

						out.print(risultato);
						%>

					</tbody>
				</table>
				<div class="row">
				<div class="col-md">
				<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#modaleInserimento">
  Inserisci
</button>
				</div>
				</div>
			</div>
		</div>
		<!-- 
		<div class="row">
			 <div class="col-md-6">
				<button type="button" class="btn btn-info btn-block" id="cta-console">Scrivi in console</button>
			</div>
			<div class="col-md-6">
				<button type="button" class="btn btn-primary btn-block" id="cta-leggi">Leggi i libri</button>
			</div>
		</div> -->


		<!--  Modale inserimento: START  -->
		<div class="modal fade" id="modaleInserimento" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Inserisci
							nuovo Store</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Riempi i campi sottostanti per inserire un nuovo store</p>

						<form action="admin/store/CreaStore" method="POST">

							<div class="form-group">
								<label for="inputNome">Nome:</label> <input type="text"
									class="form-control" id="inputNome" name="inputNome" />
							</div>
							<div class="form-group">
								<label for="inputPiva">Partita Iva:</label> <input type="text"
									class="form-control" id="inputPiva" name="inputPiva" />
							</div>
							<div class="form-group">
								<label for="inputNomeUtente">Username:</label> <input type="text"
									class="form-control" id="inputNomeUtente" name="inputNomeUtente" />
							</div>
							<div class="form-group">
								<label for="inputPassword">Password:</label> <input type="text"
									class="form-control" id="inputPassword" name="inputPassword" />
							</div>
							<div class="form-group">
								<label for="inputEmail">E mail:</label> <input type="text"
									class="form-control" id="inputEmail" name="inputEmail" />
							</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" id="cta-inserisci">Inserisci</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!--  Modale inserimento: END  -->

		<!--  Modale modifica: START  -->
		<div class="modal fade" id="modaleModifica" data-identificatore=""
			tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Aggiorna Store</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="admin/store/CreaStore" method="POST">

						<div class="form-group">
							<label for="input_nome">Nome:</label> <input type="text"
								class="form-control" id="input_nome" />
						</div>
						<div class="form-group">
							<label for="input_piva">Partita Iva:</label> <input type="text"
								class="form-control" id="input_piva" />
						</div>
						<div class="form-group">
							<label for="input_mail">Email:</label> <input type="text"
								class="form-control" id="input_mail" />
						</div>
						<div class="form-group">
							<label for="input_user">Username:</label> <input type="text"
								class="form-control" id="input_user" />
						</div>
						<div class="form-group">
							<label for="input_passw">Password:</label> <input type="text"
								class="form-control" id="input_passw" />
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="cta-inserisci">Modifica</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!--  Modale modifica: END  -->

	</div>



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>