<%@page import="com.models.Ordine"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.services.OrdineDao"%>
<%@page import="com.services.UtenteDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<title>Elenco Ordini</title>
</head>
<body>

	<div class="container">
		<h1 class="text-center">Elenco Ordini</h1>
		<div class="row">

			<div class="col-md-10">

				<table class="table">
					<thead>
						<tr>
							<th>Codice</th>
							<th>Indirizzo</th>
							<th>Nickname ordinante</th>
						</tr>
					</thead>
					<tbody id="contenuto-richiesta">

						<%
						OrdineDao sDao = new OrdineDao();
						ArrayList<Ordine> elenco = sDao.getAll();

						String risultato = "";
						for (int i = 0; i < elenco.size(); i++) {
							Ordine temp = elenco.get(i);

							risultato += "<tr data-identificatore='" + temp.getCodice() + "'>";
							//risultato += "    <td><a href='dettaglioShop.jsp?idStore=" + temp.getpIva() + ">"+temp.getpIva()+"</a></td>";
							risultato += "    <td><a href='dettaglioUtente.jsp?nick=" + temp.getCodice() + "'>" + temp.getCodice() + "</a></td>";
							risultato += "    <td>" + temp.getIndirizzo() + "</td>";
							
							UtenteDAO ut=new UtenteDAO();
							String nick=ut.getById(temp.getRefUtente()).getNickname();
							risultato += "    <td>" + nick + "</td>";
							
							risultato += "</tr>";
						}

						out.print(risultato);
						%>

					</tbody>
				</table>
				<div class="row">
				<div class="col-md">
				<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#modaleInserimento">
  Inserisci
</button>
				</div>
				</div>
			</div>
		</div>
		<!-- 
		<div class="row">
			 <div class="col-md-6">
				<button type="button" class="btn btn-info btn-block" id="cta-console">Scrivi in console</button>
			</div>
			<div class="col-md-6">
				<button type="button" class="btn btn-primary btn-block" id="cta-leggi">Leggi i libri</button>
			</div>
		</div> -->


		<!--  Modale inserimento: START  -->
		<div class="modal fade" id="modaleInserimento" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Inserisci
							nuovo Utente</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Riempi i campi sottostanti per inserire un nuovo utente</p>

						<form action="admin/utente/insertutente" method="POST">

							<div class="form-group">
								<label for="input_nome">Nome:</label> <input type="text"
									class="form-control" id="input_nome" name="input_nome" />
							</div>
							<div class="form-group">
								<label for="input_cognome">Cognome:</label> <input type="text"
									class="form-control" id="input_cognome" name="input_cognome" />
							</div>
							<div class="form-group">
								<label for="input_nickname">Username:</label> <input type="text"
									class="form-control" id="input_nickname" name="input_nickname" />
							</div>
							<div class="form-group">
								<label for="input_password">Password:</label> <input type="text"
									class="form-control" id="input_password" name="input_password" />
							</div>
							<div class="form-group">
								<label for="input_email">E mail:</label> <input type="text"
									class="form-control" id="input_email" name="input_email" />
							</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" id="cta-inserisci">Inserisci</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!--  Modale inserimento: END  -->


	</div>



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>