<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.services.UtenteDAO"%>
<%@page import="com.models.Utente"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifica Shop</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">

				<%
				String nick = (String) request.getParameter("nick");

				UtenteDAO sdao = new UtenteDAO();
				Utente temp = sdao.getById(sdao.getByUser(nick).getId_utente());
				%>

				<form action="admin/utente/updateutente" method="POST">
					<h1>Dettaglio Utente</h1>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="nome">Nome:</label> <input type="text"
									class="form-control" id="nome" name="nome"
									value="<%out.print(temp.getNome());%>" />

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="cognome">Cognome:</label> <input type="text"
									class="form-control" id="cognome" name="cognome"
									value="<%out.print(temp.getCognome());%>" />
							</div>
						</div>
					</div>

	<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="nickname">Username:</label> <input type="text"
				 					class="form-control" id="nickname" name="nickname"
									value="<%out.print(temp.getNickname());%>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="password">Password:</label> <input type="text"
									class="form-control" id=password name="password"
									value="<%out.print(temp.getPassword());%>" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="email">Email:</label> <input type="text"
									class="form-control" id="email" name="email"
									value="<%out.print(temp.getEmail());%>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="ruolo">Ruolo:</label> <input type="text"
									class="form-control" id="ruolo" name="ruolo"
									value="<%out.print(temp.getRuolo());%>" />
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
					<div class="form-group">
								<label for="idUtente">id:</label> 
								<input type="text"
									class="form-control" id="idUtente" name="idUtente"
									value="<%out.print(temp.getId_utente());%>" />
							</div>
					</div>
					</div>

				
					<div class="row">
						<div class="col-md">
							<button type="submit" class="btn btn-warning btn-block"
								id="cta-inserisci">Modifica</button>
						</div>
					</div>
				</form>

				<div class="row">
					<div class="col-md">
						<form action="Admin/utente/delete" method="POST">
							<input type="text" class="form-control" id="idUtente" name="idUtente"
								value="<%out.print(temp.getId_utente());%>" />
							<div class="row">
								<div class="col-md">
									<button type="submit" class="btn btn-danger btn-block"
										id="cta-inserisci">Elimina</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>

		<%-- <div class="row">
			<div class="col">
				<%
					/* String nome = "Giovanni";
				
					out.print(nome); */
					
					/* HttpSession sessione = request.getSession();
			
					out.print(sessione.getAttribute("stud_id"));
					out.print(sessione.getAttribute("stud_nome"));
					out.print(sessione.getAttribute("stud_cognome"));
					out.print(sessione.getAttribute("stud_matricola"));
					out.print(sessione.getAttribute("stud_sesso")); */
					
					
				%>
				
				
			</div>
		</div>	 --%>
	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>